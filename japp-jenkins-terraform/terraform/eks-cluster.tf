terraform {
    required_version = ">=0.12"
    backend "s3" {
        bucket = "myapp-bucket2022"
        key = "myapp/state.tfstate"
        region = "ap-southeast-2"
    }
}
variable k8s_version {}
variable env_prefix {}
variable cluster_name {}
variable region {}
module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "18.0.4"

  cluster_name = local.cluster_name
  cluster_version = var.k8s_version

  subnet_ids = module.myapp-vpc.private_subnets
  vpc_id = module.myapp-vpc.vpc_id

  tags = {
    Environment = "bootcamp"
    Terraform = "true"
  }

  eks_managed_node_groups = {
      dev = {
      min_size     = 1
      max_size     = 3
      desired_size = 3
      instance_types = ["t2.micro"]
      labels = {
        Environment = var.env_prefix
      }
    }     
  }
  fargate_profiles = [{
        name = "fargate-profile-1"
        selectors = [
            {
            namespace = "my-app"
            }
        ]
  }]

}